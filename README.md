This is a routine to use AnalyticLC (https://github.com/yair111/AnalyticLC) with python to calculate physical and orbit properties of exoplanets. It uses dynesty for nested sampling and has a few tweeks to translate different orbit parameter defintions. The files in AnalyticLC, FittingAndMath, FluxCalculation and LaplaceCoeffs are by Yair Judkowski, I only added AnalyticLC/PreForcedElements.

To use this code, your system needs to be able to run octave. This is installable with ever common packet manager.
Also, to run it you have to change one of the first lines to your own working directory.

Have fun :)

Things to remember:
- Sometimes AnalyticLC breaks with Inclinations = 0 and Omega = 0, this can be worked around with small numbers
- the eccentricities should not get too big as this leads to unreliable results
- the Radial Velocities are returned in RV/R_Star

