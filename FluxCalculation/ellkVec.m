function ellk=ellkVec(k)
% Computes polynomial approximation for the complete elliptic
% integral of the first kind (Hasting's approximation):
m1=1-k.*k;
m1(m1<eps)=eps;
a0=1.38629436112;
a1=0.09666344259;
a2=0.03590092383;
a3=0.03742563713;
a4=0.01451196212;
b0=0.5;
b1=0.12498593597;
b2=0.06880248576;
b3=0.03328355346;
b4=0.00441787012;
ek1=a0+m1.*(a1+m1.*(a2+m1.*(a3+m1*a4)));
ek2=(b0+m1.*(b1+m1.*(b2+m1.*(b3+m1*b4)))).*log(m1);
ellk=ek1-ek2;
