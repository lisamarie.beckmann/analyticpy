function ellec=ellecVec(k)
% Computes polynomial approximation for the complete elliptic
% integral of the second kind (Hasting's approximation):
m1=1-k.*k;
m1(m1<eps)=eps;
a1=0.44325141463;
a2=0.06260601220;
a3=0.04757383546;
a4=0.01736506451;
b1=0.24998368310;
b2=0.09200180037;
b3=0.04069697526;
b4=0.00526449639;
ee1=1+m1.*(a1+m1.*(a2+m1.*(a3+m1*a4)));
%ee2=m1.*(b1+m1.*(b2+m1.*(b3+m1.*b4))).*log(1./m1);
ee2=m1.*(b1+m1.*(b2+m1.*(b3+m1.*b4))).*(-1*log(m1));
ellec=ee1+ee2;
