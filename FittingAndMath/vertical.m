function vertvec=vertical(vec)
%reshape input vector into row vector
    
if min(size(vec))>1
    vertvec=[];
    return
end

vertvec=reshape(vec,length(vec),1);