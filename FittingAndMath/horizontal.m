function horzvec=horizontal(vec)
%reshape input vector into row vector
    
if min(size(vec))>1
    horzvec=[];
    return
end

horzvec=reshape(vec,1,length(vec));