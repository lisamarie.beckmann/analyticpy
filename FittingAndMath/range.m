function res = range(X)
res=max(X(:))-min(X(:));
return