function xt = Vector1stOrderDE(A,x0,t)
%Vector1stOrderDE: - solves the vector equation dx/dt=Ax

%inputs: A - matrix of the equation, x0 - x for t=0, t - time axis to
%evaluate values at. derivation is given at:
%https://www.unf.edu/~mzhan/chapter4.pdf

%Yair Judkovsky, 3.10.2020

if nargin==0
A=[.1,.01;-.002,-.002].*i;
x0=[.3,0.0001];
t=linspace(0,100,100000);
end
nt=length(t);
if any(~isfinite(A(:))), xt = 123.123; return, end
[ev,lam]=eig(A);
nev=length(diag(lam));
phi0i=inv(ev);
% add singleton dimension as the first (time) index and replicate
% eigenvectors along that dimension.
evp=repmat(reshape(ev,[1,nev,nev]),[length(t),1,1]);

lamt=reshape(exp(diag(lam)*t).',length(t),1,nev);
lamt=repmat(lamt,1,nev,1);
phit=lamt.*evp;

xt=reshape(reshape(phit,nev*nt,nev)*(phi0i*x0),nt,nev);

if nargin==0
figure;
plot(xt);
hold on
plot(real(x0),imag(x0),'o');
axis equal
end

