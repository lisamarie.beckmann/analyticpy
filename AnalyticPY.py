#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
abgewandelt von:
Created on Fri Dec  3 08:49:07 2021

@author: dreizler
"""
from scipy.stats import gamma,norm,beta,truncnorm
import scipy.linalg as sl
from numpy.matlib import repmat
import numpy as np

from types import SimpleNamespace

# nested sampling and multiprocessing tools
import dynesty
from dynesty.utils import resample_equal
from dynesty import plotting as dyplot
from multiprocessing import Pool


# system functions that are always useful to have
import time, sys, os

# basic numeric setup
import numpy as np
from numpy import random
# inline plotting
#%matplotlib inline

# running matlab
from oct2py import Oct2Py

# plotting
import matplotlib
from matplotlib import pyplot as plt

import csv


# managing data
import json
import pickle

#########################global things##########################################
OctaveDir = '/home/lisa/analyticlctoflexifit/' #path to directory with AnalyticLC (dir above)


################################################################################
##########################Import data###########################################
################################################################################

def parse_parameters(settings):
    """
    reads from dictionaries in settings to get which parameters shall be fixed/determined with dynesty
    returns:
    p: list with all parameters that should be modeled around with ranges
    fix: list with fixed values 
    """
    p = []
    fix = {}
    for planet in settings["planets"]:
        for key, value in planet.items():
            fix[key] = [(None, None)] * len(settings["planets"])

    i = 0
    for planet in settings["planets"]:
        for key, value in planet.items():
            if value["type"] != "fixed":
                p += [(i, key, value["type"], value["values"])]
            else:
                fix[key][i] = value["values"]
        i += 1

    for key, value in settings["parameters"].items():
        if value["type"] != "fixed":
            p += [(-1, key, value["type"], value["values"])]
        else:
            fix[key] = value["values"]

    return p, fix

def parse_settings():
    """
    read in json with parameters to be fixated or modeled around
    returns the ranges to model, fixed parameters and settings like number of livepoints and datapaths
    """
    ettings = json.load(open("parameter.json"))

    p, fix = parse_parameters(settings)

    return p, fix, settings["nkern"], settings["nlivepoints"], settings["RV_datapath"], settings["LC_datapath"], settings["save_datapath"]

def import_data(RV_datapath, TR_datapath):
    """
    reads in RV and TR data if a datapath is given. Standard data path with no data of that type: "/dev/null"
    returns list with time, data and err of each
    """
    if RV_datapath == "/dev/null":
        tRV = []
        RV = []
        RVerr = []
    else:
        tRV, RV, RVerr = np.loadtxt(RV_datapath, unpack=True)
        S_R = 0.5*6.957e8 #stellar radii in meters  #todo: put in settings
        d = 86400 #seconds in a day
        RV = RV/(S_R/d) #RV_o_R output from AnalyticLC in stellar radii over time unit (days)
        RVerr /= S_R/d
    if TR_datapath == "/dev/null":
        t = []
        TR = []
        TRerr = []
    else:
        t, TR, TRerr = np.loadtxt(TR_datapath, delimiter = " ", unpack=True)

    return tRV, RV, RVerr, t, TR, TRerr

################################################################################
#################Translate parameters###########################################
################################################################################

def z_to_eom(z): 
    """
    takes the complex eccentricity z of format z=e*exp(i*omega) with forced parts
    (so after using function geteccRV/geteccT)
    and returns omega and e
    
    attention: the complex ecc are given for every point in t/tRV,
    I decided for my use-cases that it is sufficent to take z at the first time point,
    it may be different for yours
    """
    elist = []
    omlist = []
    for single_z in z[0]:
        h = np.real(single_z) #z is a list, given for every evaluated time point
        k = np.imag(single_z) #decided to take the configuration at the beginning
        hksq = np.sqrt(h**2+k**2)
        if k==0:
            e = -h
            om = np.pi
        elif(hksq != 0 and h*hksq != h**2+k**2):
            e = hksq
            om = 2*(np.arctan(k/(h-hksq)))
        else:
            e = hksq
            om = 2*(np.arctan(k/(hksq+h)))
        elist.append(e)
        omlist.append(om)

    return elist,omlist


def calc_peri(Tmid, P, ecc, omega):
    """
    caclulates the periastron time from time of mid transit, period,
    eccentricity and argument of periapsis
    """
    f = np.pi/2 -omega
    ee = 2 * np.arctan(np.tan(f/2) * np.sqrt((1-ecc)/(1+ecc)))
    tperi = Tmid - P/(2*np.pi)*(ee-ecc*np.sin(ee))
    return tperi-Tmid

def getecc(P,Tmid0,mu,ex0,ey0,I0,Omega0,Lambda,tToRV,t,MaxPower=4):
    """
    help function that calculates the complex eccentricty and inclination
    including free and forced elements
    """
    OrdersAll=len(P)
    init_octpy()
    free_e_T,free_I_T  = octave.feval("PreForcedElements",P,mu,ex0,ey0,I0,Omega0,tToRV,t,nout=2) 
    print("free_e_T {}", free_e_T)
    print("free_I_T {}", free_I_T)
    #todo: more than 2 planets
    #dz = forced eccentricities, dLambda=forced mean longitudes, du = forced inclinations, da_oa = 
    dz,dLambda,du,da_oa = octave.feval("ForcedElements1",P, Lambda, mu, free_e_T, free_I_T,OrdersAll,MaxPower,nout=4)
    z = free_e_T + dz
    u = free_I_T +du
    #Lambda = free lambda +dLambda

    return (z,u)

def geteccRV(P, Tmid0, mu, ex0,ey0,I0,Omega0,tRV,t,MaxPower=4):
    """
    calculates the complex eccentricty and inclination including free and forced
    elements for every point in tRV
    uses function getecc
    """
    n = 2*np.pi/np.asarray(P)
    NRV = len(tRV)
    t0_RV = np.zeros((len(tRV),len(Tmid0)))
    for i in range(len(Tmid0)):
        t0_RV[:,i] = tRV-Tmid0[i]
    print(t0_RV)
    Lambda_RV = t0_RV*repmat(n,NRV,1)
    Lambda_RV.reshape(-1)
    z,u = getecc(P,Tmid0,mu,ex0,ey0,I0,Omega0,Lambda_RV,tRV,t,MaxPower)
    return (z,u)

def geteccT(P,Tmid0,mu,ex0,ey0,I0,Omega0,tT,t,MaxPower=4):
    """
    calculates the complex eccentricty and inclination including free and forced
    elements for every point in tRV
    uses function getecc
    """
    n = 2*np.pi/np.asarray(P)
    NTr=len(tT)
    t0_T=np.zeros((len(tT),len(Tmid0)))
    for i in range(len(Tmid0)):
        t0_T[:,i]=tT[i]-Tmid0[i]
    print(t0_T)
    Lambda_T=t0_T*repmat(n,NTr,1)
    Lambda_T.reshape(-1)
    z,u = getecc(P,Tmid0,mu,ex0,ey0,I0,Omega0,Lambda_T,tT,t,MaxPower)
    return (z,u)

    
################################################################################
################Dynesty and Octave enviroment###################################
################################################################################

def calculate(nkern, nlivepoints, parameters, fixed_parameters, savedatapath):
    """
    the main dynesty function: here we calculate the results and do the multiprocessing/pooling
    returns the dynesty sampler result object
    saves a pickle file to be able to run code with the dynesty results
    """
    ndim = len(parameters)
    p = Pool(nkern, init, [parameters, fixed_parameters]) 

    #P, Tmid0, ror, aor, mu, ex0, ey0, I0, Omega0, sig
    dsampler = dynesty.NestedSampler(loglike, prior_transform, ndim=ndim, periodic=None,
            bound='multi', sample='auto',nlive=nlivepoints,pool=p, queue_size=nkern) #n*n/2 live points mind.
    dsampler.run_nested(dlogz=0.001)
    dres = dsampler.results
    with open(f"{savedatapath}/restart.pickle", 'wb') as f:
        pickle.dump(dres, f, pickle.HIGHEST_PROTOCOL)	
    return dres
    

def init(parameters, fixed_parameters):
    """
    initalizes global parameter and later octave instances
    for pooling in function calculate()
    """
    global p 
    p = parameters
    global fix
    fix = fixed_parameters
    init_octpy()

def init_octpy():
    """
    initializes octave instances
    """
    global octave
    octave = Oct2Py()
    BaseDir=OctaveDir
    octave.addpath(BaseDir)
    octave.addpath(BaseDir + 'AnalyticLC/')
    octave.addpath(BaseDir + 'FittingAndMath/')
    octave.addpath(BaseDir + 'FluxCalculation/')
    octave.LaplaceCoeffs('load',BaseDir + 'LaplaceCoeffs.mat')

#################### Prior transforms for nested samplers ######################
def transform_uniform(x, hyperparameters):
    a, b = hyperparameters
    return a + (b-a)*x

def transform_loguniform(x, hyperparameters):
    a, b = hyperparameters
    la = np.log(a)
    lb = np.log(b)
    return np.exp(la + x * (lb - la))

def transform_normal(x, hyperparameters):
    mu, sigma = hyperparameters
    return norm.ppf(x, loc=mu, scale=sigma)

def transform_beta(x, hyperparameters):
    a, b = hyperparameters
    return beta.ppf(x, a, b)

def transform_exponential(x, hyperparameters):
    a = hyperparameters
    return gamma.ppf(x, a)

def transform_truncated_normal(x, hyperparameters):
    mu, sigma, a, b = hyperparameters
    ar, br = (a - mu) / sigma, (b - mu) / sigma
    return truncnorm.ppf(x, ar, br, loc=mu, scale=sigma)

def transform_modifiedjeffreys(x, hyperparameters):
    turn, hi = hyperparameters
    return turn * (np.exp( (x + 1e-10) * np.log(hi/turn + 1)) - 1)

def prior_transform(utheta):
    global p;
    global fix;
    
    transforms = [x[2] for x in p]
    hyperparams = [x[3] for x in p]
    transformed_priors = np.zeros(len(utheta))
    for k,param in enumerate(utheta):
        if transforms[k] == 'uniform':
            transformed_priors[k] = transform_uniform(param,hyperparams[k])
        elif transforms[k] == 'loguniform':
            transformed_priors[k] = transform_loguniform(param,hyperparams[k])
        elif transforms[k] == 'normal':
            transformed_priors[k] = transform_normal(param,hyperparams[k])
        else:
            raise ValueError("Transform not known")

    return transformed_priors


def namespace(theta, p, fix):
    parameters = fix.copy()
    for i in range(len(theta)):
        planetid = p[i][0]
        keyname = p[i][1]
        if planetid == -1:
            parameters[keyname] = theta[i]
        else:
            parameters[keyname][planetid] = theta[i]

    n = SimpleNamespace(**parameters)

    return n, parameters


def loglike(theta):
    """
    calculates the log-likelihood based on the AnalyticLC model
    returns -inf in case errors occur
    """
    global octave;
    global p;
    global fix;

    n, parameters = namespace(theta, p, fix)
        
    try:
        LC,RV_o_r,Y_o_r,Z_o_r,O = octave.feval("AnalyticLC",n.P, n.Tmid0, n.ror, n.aor, n.mu, n.ex0, n.ey0, n.I0, n.Omega0, n.t, n.u1, n.u2,'tRV',n.tRV, nout=5)
        ret = 0
        
        if len(n.RV)!= 0:  #in case of RV data
            modelRV = RV_o_r.reshape(-1)
            inv_sigmaRV = 1.0/(n.RVerr**2 + np.exp( 2 * n.sigRV))
            ret += -0.5 * (np.sum( (n.RV-modelRV)**2 * inv_sigmaRV - np.log(inv_sigmaRV)))
            
        if len(n.TR) != 0:  #in case of transit data
            modelLC = LC.reshape(-1)
            inv_sigmaLC = 1.0/(n.TRerr**2 + np.exp( 2 * n.sigTR))
            ret += -0.5 * (np.sum( (n.TR-modelLC)**2 * inv_sigmaLC - np.log(inv_sigmaLC)))
        
        return ret
        
    except Exception as e:
        #print(n.P, n.Tmid0, n.ror, n.aor, n.mu, n.ex0,n.ey0, n.I0, n.Omega0,n.t, n.u1, n.u2, n.tRV)
        print(e)
        return -np.inf
        
################################################################################
######################### Plotting of results ##################################
################################################################################

def create_plot(dres,p,fix,ndim,savedatapath):

    #global p;
    #truths = [P_true[0], P_true[1], sig_true]
    #labels = [x[1] for x in p]
    
    ###################### traceplot ###########################################
    labels = [r'P $d$',r'T $d$', r'ex0 ',r'ey0 ',  r'mu', r'sigRV', r'aor', r'Omega0',r'$\sigma \log(S_R/s)$']
    fig, axes = dyplot.traceplot(dres, labels=labels, connect=True,
            fig=plt.subplots(ndim, 2))#, constrained_layout=True))
    fig.tight_layout()
    plt.savefig(f"{savedatapath}/traceplot.png")
    plt.show()
    plt.close()
    
    
    ###################### corner plot #########################################
    
    fig, axes = dyplot.cornerplot(dres, show_titles=True, 
            title_kwargs={'y': 1.04}, labels=labels,
            fig=plt.subplots(ndim, ndim))#,constrained_layout=True))
    fig.tight_layout()
    plt.savefig(f"{savedatapath}/cornerplot.png")
    plt.show()
    plt.close()
    solu = dres["samples"][dres["niter"]]
    
    
    ##################### lightcurve and RV ####################################
    
    n, parameters = namespace(solu, p, fix)
    init_octpy()
    tRV = np.linspace(n.tRV[0], n.tRV[-1], 1000)
    #t = np.linspace(n.t[0], n.t[-1], 1000)
    LC,RV_o_r,Y_o_r,Z_o_r,O = octave.feval("AnalyticLC",n.P, n.Tmid0, n.ror, n.aor, n.mu, n.ex0, n.ey0, n.I0, n.Omega0, n.t, n.u1, n.u2,'tRV',tRV, nout=5)
    print(n.P, n.Tmid0, n.ror, n.aor, n.mu, n.ex0,n.ey0, n.I0, n.Omega0,n.t, n.u1, n.u2, n.tRV)
    if len(n.TR)!=0:
        LC = LC.reshape(-1)
        plt.plot(t, LC, label = 'model')
        plt.errorbar(n.t, n.TR, yerr=n.TRerr, fmt = 'o')
        plt.legend()
        plt.xlabel("time [d]")
        plt.ylabel("flux noramlized")
        plt.savefig(f"{savedatatpah}/LC_final.png")
        plt.show()
        plt.close()

        #phase folded
        for P in n.P:
            plt.plot((t-t[0]+0.5*P)%P+0.5*P, LC, 'o', ms = 0.4, label = 'model')
            plt.errorbar((n.t-n.t[0]+0.5*P)%P+0.5*P, n.TR, yerr = n.TRerr, fmt='o')
            plt.xlabel("phase")
            plt.ylabel("flux normalized")
            plt.legend()
            plt.savefig(f"{savedatapath}/LC_phase_{P}.png")
            plt.show()
            plt.close()
            #to do: snip around every transit -> save plots
    if len(n.RV)!=0:
        RV_o_r= RV_o_r.reshape(-1)
        plt.plot(tRV, RV_o_r, 'o', label = 'model')
        plt.errorbar(n.tRV, n.RV, yerr = n.RVerr, fmt ='o')
        plt.legend()
        plt.xlabel("time [d]")
        plt.ylabel("RV [S_R/d]")
        plt.savefig(f"{savedatapath}/RV_final.png")
        plt.show()
        plt.close()

        #phase folded
        for P in n.P:
            plt.plot((tRV-tRV[0])%P, RV_o_r, 'o', ms = 0.4, label = 'model')
            plt.errorbar((n.tRV-n.tRV[0])%P, n.RV, yerr = n.RVerr, fmt='o')
            plt.xlabel("phase")
            plt.ylabel("RV [S_R/d]")
            plt.legend()
            plt.savefig(f"{savedatapath}/RV_phase_{P}.png")
            plt.show()
            plt.close()
            



################################################################################
####################### Main modeling function #################################
################################################################################

def main():    
    parameters, fixed, nkern, nlivepoints, RVdatapath, TRdatapath, savedatapath = parse_settings()
    
    ndim = len(parameters)
    
    tRV, RV, RVerr, t, TR, TRerr = import_data(RVdatapath, TRdatapath)
    
    fixed["RV"] = RV
    fixed["RVerr"] = RVerr
    fixed["tRV"] = tRV
    fixed["TR"] = TR
    fixed["TRerr"] = TRerr
    fixed["t"] = t

    #in case you want to use previously calculated data: use line with pickle
    results = calculate(nkern, nlivepoints, parameters, fixed, savedatapath)
    #results = pickle.load(open(f"{savedatapath}/restart.pickle", "rb")) 
    
    create_plot(results, parameters, fixed, ndim, savedatapath)
    

main()
