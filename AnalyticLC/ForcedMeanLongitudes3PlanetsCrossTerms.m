function dLambda = ForcedMeanLongitudes3PlanetsCrossTerms(P,Lambda,mu,z)
% ForcedMeanLongitudes3Planets: calculate the forced mean longitudes of a set of
% planets due to the cross-terms among different resonances.

%Inputs: P - orbital periods, Lambda - mean longitudes, mu - planets/star
%mass ratio, z - free eccentricities (if they are not given, zero values
%are assumed). Labmda and z can be given as columns of multiple values.

% Yair Judkovsky, 2.11.2020

if ~exist('z','var')
    z = zeros(size(P));
end

%mean motions
n = 2*pi./P;

%absolute values and angles of eccentricities
pom = angle(z);
e = abs(z);
zx = real(z);
zy = imag(z);

%allocate space for 3d matrices that will hold the values of dLambda
%according to these dimensios: orders, data points, number of planets
dLambda = zeros(size(z,1),length(P));

%go over all possible planets triplets
for j1 = 1:length(P)
    for j2 = (j1+1):length(P)
        for j3 = (j2+1):length(P)
            
            %calculate the nearest 1st order MMR for each pair in the
            %triplet
            j = round(P(j2)/(P(j2)-P(j1)));
            k = round(P(j3)/(P(j3)-P(j2)));
            nj = j*n(j2)+(1-j)*n(j1);
            nk = k*n(j3)+(1-k)*n(j2);
            alph12 = (n(j2)/n(j1))^(2/3)*((1+mu(j1))/(1+mu(j2)))^(1/3);
            alph23 = (n(j3)/n(j2))^(2/3)*((1+mu(j2))/(1+mu(j3)))^(1/3);
            
            %longitudes of conjunction
            Lambda_j = j*Lambda(:,j2)+(1-j)*Lambda(:,j1);
            Lambda_k = k*Lambda(:,j3)+(1-k)*Lambda(:,j2);
            
            %laplace coefficients
            Ak = LaplaceCoeffs(alph23,0.5,k,0);
            DAk = LaplaceCoeffs(alph23,0.5,k,1);
            Ajm1 = LaplaceCoeffs(alph12,0.5,j-1,0);
            DAjm1 = LaplaceCoeffs(alph12,0.5,j-1,1);
            D2Ajm1 = LaplaceCoeffs(alph12,0.5,j-1,2);
            f27k = -k*Ak-0.5*alph23*DAk;
            f31j = (j-0.5)*Ajm1+0.5*alph12*DAjm1;
            
            Df31j = (j-0.5)*DAjm1+0.5*DAjm1+0.5*alph12*D2Ajm1;
            
             F=sqrt(1);            
            %calculate and add to the dlambda of the intermediate planet
            AlphaFactor = -3/2*f27k*(f31j-(j==2)/(2*alph12^2))*alph23;
            MassFactor = mu(j1)*mu(j3)/(1+mu(j2))^2;            
            SinTerm1 = (j/nk+(1-k)/nj)*sin(Lambda_j+Lambda_k-2*pom(:,j2))/(nj+nk)^2;
            SinTerm2 = (j/nk-(1-k)/nj)*sin(Lambda_j-Lambda_k)/(nj-nk)^2;                       
            dLambda(:,j2) = dLambda(:,j2)+F*AlphaFactor*MassFactor*n(j2)^3*(SinTerm1+SinTerm2);
        
            %calculate and add to the dlambda of the innermost planet
            AlphaFactor = 3/2*f27k*(f31j-2*alph12*(j==2))*alph12*alph23;
            MassFactor = mu(j2)*mu(j3)/(1+mu(j1))/(1+mu(j2));
            SinTerm1 = sin(Lambda_j+Lambda_k-2*pom(:,j2))/(nj+nk)^2;
            SinTerm2 = sin(Lambda_j-Lambda_k)/(nj-nk)^2;
            dLambda(:,j1) = dLambda(:,j1)+F*AlphaFactor*MassFactor*n(j1)^2*n(j2)*(j-1)/nk*(SinTerm1+SinTerm2);
              
            %calculate and add to the dlambda of the outermost planet
            AlphaFactor = -3/2*f27k*(f31j-(j==2)/(2*alph12^2));
            MassFactor = mu(j1)*mu(j2)/(1+mu(j2))/(1+mu(j3));
            SinTerm1 = sin(Lambda_j+Lambda_k-2*pom(:,j2))/(nj+nk)^2;
            SinTerm2 = -sin(Lambda_j-Lambda_k)/(nj-nk)^2;
            dLambda(:,j3) = dLambda(:,j3)+F*AlphaFactor*MassFactor*n(j3)^2*n(j2)*k/nj*(SinTerm1+SinTerm2);
            

        end
    end
end
