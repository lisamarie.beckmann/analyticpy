function [w,d,b,T,Tau] = OrbitalElements2TransitParams(n,aor,ex,ey,I,Omega,r)
%OrbitalElements2TransitParams: returns the duration, ingress-egress time and angular velocity as a
%function of the orbital properties.

%inputs: n - mean motion, aor - semi-major axis over stellar radius, ex,ey - eccentricity vector components, x
%pointing at observer, I,Omega - inclination with respect to the planet xy,
%r - planetary radius in units of stellar radii.
%where x points at observer. Outputs: T=Duration, Tau=ingress/egress time,
%w = angular velocity at mid-transit, d = planet-star distance at
%mid-transit, b = impact parameter

%Yair Judkovsky, 9.9.2020

w = n.*(1+ex).^2./(1-ex.^2-ey.^2).^(3/2);
d = aor.*(1-ex.^2-ey.^2)./(1+ex);
b = d.*sin(I).*sin(Omega);

if nargout>3
asin0 = asin(sqrt(((1+ex).^2./(aor.^2.*(1-ex.^2-ey.^2).^2)-sin(I).^2.*sin(Omega).^2)./(1-sin(I).^2.*sin(Omega).^2)));
T=1./n*2.*((1-ex.^2-ey.^2).^(3/2))./((1+ex).^2).*asin0;
asin2 = asin(sqrt(((1+r).^2.*(1+ex).^2./(aor.^2.*(1-ex.^2-ey.^2).^2)-sin(I).^2.*sin(Omega).^2)./(1-sin(I).^2.*sin(Omega).^2)));
asin1 = asin(sqrt(((1-r).^2.*(1+ex).^2./(aor.^2.*(1-ex.^2-ey.^2).^2)-sin(I).^2.*sin(Omega).^2)./(1-sin(I).^2.*sin(Omega).^2)));
Tau=1./n.*((1-ex.^2-ey.^2).^(3/2))./((1+ex).^2).*(asin2-asin1);
end