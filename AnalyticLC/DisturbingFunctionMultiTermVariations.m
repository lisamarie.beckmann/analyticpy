function [dLambda1,dLambda2,dz1,dz2,du1,du2,da_oa1,da_oa2] = DisturbingFunctionMultiTermVariations(n1,n2,alph,Lambda1,Lambda2,mu1,mu2,j,k,e1,e2,Pom1,Pom2,I1,I2,s1,s2,Om1,Om2,A,Ap,B,Bp,C,Cp,D,Dp,f,fE,fI,Df,DfE,DfI)

%get the number of time stamps and number of terms, in order to replicate
%the vectors and perform the calculation once for all terms
% Ntime = length(Lambda1);
% Nterm = length(k);
% 
% 
% Lambda1 = repmat(Lambda1,1,Nterm);
% Lambda2 = repmat(Lambda2,1,Nterm);
% e1 = repmat(e1,1,Nterm);
% e2 = repmat(e2,1,Nterm);
% Pom1 = repmat(Pom1,1,Nterm);
% Pom2 = repmat(Pom2,1,Nterm);
% I1 = repmat(I1,1,Nterm);
% I2 = repmat(I2,1,Nterm);
% s1 = repmat(s1,1,Nterm);
% s2 = repmat(s2,1,Nterm);
% Om1 = repmat(Om1,1,Nterm);
% Om2 = repmat(Om2,1,Nterm);
% 
% if length(j)==length(k)
%     j = repmat(j,Ntime,1);
% end
% k = repmat(k,Ntime,1);
% A = repmat(A,Ntime,1);
% Ap = repmat(Ap,Ntime,1);
% B = repmat(B,Ntime,1);
% Bp = repmat(Bp,Ntime,1);
% C = repmat(C,Ntime,1);
% Cp = repmat(Cp,Ntime,1);
% D = repmat(D,Ntime,1);
% Dp = repmat(Dp,Ntime,1);

f_inner = f+fE;
f_outer = f+fI;
Df_inner = Df+DfE;
Df_outer = Df+DfI;

% f = repmat(f,Ntime,1);
% fE = repmat(fE,Ntime,1);
% fI = repmat(fI,Ntime,1);
% Df = repmat(Df,Ntime,1);
% DfE = repmat(DfE,Ntime,1);
% DfI = repmat(DfI,Ntime,1);

% f_inner = repmat(f_inner,Ntime,1);
% f_outer = repmat(f_outer,Ntime,1);
% Df_inner = repmat(Df_inner,Ntime,1);
% Df_outer = repmat(Df_outer,Ntime,1);

SQ1 = sqrt(1-e1.^2);
SQ2 = sqrt(1-e2.^2);

%calculate the cosine argument
Phi = j.*Lambda2+(k-j).*Lambda1-C.*Pom1-Cp.*Pom2-D.*Om1-Dp.*Om2;

%calculate Cjk and Sjk
PowersFactor = e1.^A.*e2.^Ap.*s1.^B.*s2.^Bp;
Cjk = PowersFactor.*cos(Phi);
Sjk = PowersFactor.*sin(Phi);

%calculate the mean-motion of the longitude of conjunction
njk = j*n2+(k-j)*n1;

%orbital elements variations - inner planet
da_oa1 = 2*mu2/(1+mu1)*alph*(f_inner).*(k-j)*n1./njk.*Cjk;

dLambda1 = mu2/(1+mu1)*alph*n1./njk.*Sjk...
    .*(...
    3.*(f_inner).*(j-k)*n1./njk...
    -2*alph*(Df_inner)...
    +(f_inner).*A.*(SQ1.*(1-SQ1))./(e1.^2)...
    +(f_inner).*B/2./SQ1...
);

de1 = mu2/(1+mu1)*alph*(f_inner).*SQ1./e1*n1./njk.*Cjk...
    .*((j-k).*(1-SQ1)+C);

dPom1 = mu2/(1+mu1)*alph*(f_inner)*n1./njk.*Sjk...
    .*(A.*SQ1./(e1.^2)+B/2./SQ1);

dI1 = mu2/(1+mu1)*alph*(f_inner)./SQ1*n1./njk.*Cjk...
    .*((j-k+C).*tan(I1/2)+D./sin(I1));

dOm1 = mu2/(1+mu1)*alph*(f_inner).*B/2.*cot(I1/2)./(SQ1.*sin(I1))*n1./njk.*Sjk;

% dI1 = 0; dOm1 = 0;



%orbital elements variations - outer planet
da_oa2 = 2*mu1/(1+mu2)*(f_outer).*j*n2./njk.*Cjk;

dLambda2 = mu1/(1+mu2)*n2./njk.*Sjk.*...
    (...
  -3*(f_outer).*j*n2./njk...
  +2*(f_outer+alph*Df_outer)...
  +(f_outer).*Ap.*SQ2.*(1-SQ2)./(e2.^2)...
  +(f_outer).*Bp/2./SQ2...
);

de2 = mu1/(1+mu2)*(f_outer).*SQ2./e2*n2./njk.*Cjk.*...
    (...
    -j.*(1-SQ2)+Cp...
);

dPom2 = mu1/(1+mu2)*(f_outer)*n2./njk.*Sjk.*...
    (...
  Ap.*SQ2./(e2.^2)+Bp/2./SQ2...  
);

dI2 = mu1/(1+mu2)*(f_outer)./SQ2*n2./njk.*Cjk.*...
    (...
  (Cp-j).*tan(I2/2)+Dp./sin(I2)...  
);

dOm2 = mu1/(1+mu2)*(f_outer).*Bp/2.*cot(I2/2)./(SQ2.*sin(I2))*n2./njk.*Sjk;

% dI2 = 0; dOm2 = 0;

NewVersion = 1;


if ~NewVersion
%translate to the complex form
dz1 = (de1+1i*dPom1.*e1).*exp(1i*Pom1);
dz2 = (de2+1i*dPom2.*e2).*exp(1i*Pom2);

du1 = (dI1+1i*dOm1.*I1).*exp(1i*Om1);
du2 = (dI2+1i*dOm2.*I2).*exp(1i*Om2);


%sum the distribution of all terms to get the total effect
dz1 = sum(dz1,2);
dz2 = sum(dz2,2);
du1 = sum(du1,2);
du2 = sum(du2,2);
da_oa1 = sum(da_oa1,2);
da_oa2 = sum(da_oa2,2);
dLambda1 = sum(dLambda1,2);
dLambda2 = sum(dLambda2,2);

else
   %translate to the complex form
dz1 = (sum(de1,2)+1i*sum(dPom1,2).*e1).*exp(1i*Pom1);
dz2 = (sum(de2,2)+1i*sum(dPom2,2).*e2).*exp(1i*Pom2); 
du1 = (sum(dI1,2)+1i*sum(dOm1,2).*I1).*exp(1i*Om1);
du2 = (sum(dI2,2)+1i*sum(dOm2,2).*I2).*exp(1i*Om2);
da_oa1 = sum(da_oa1,2);
da_oa2 = sum(da_oa2,2);
dLambda1 = sum(dLambda1,2);
dLambda2 = sum(dLambda2,2);
end