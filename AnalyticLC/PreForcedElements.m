function [free_e_T, free_I_T] = PreForcedElements(P,mu,ex0,ey0,I0,Omega0,tT,t)
%prepare parameters for ForcedElements1, mainly calculate the free matrices

%define the imaginary free eccentricity and inclination
free_e_0 = ex0+1i*ey0;
free_I_0 = I0.*exp(1i.*Omega0);

%define the secular interatction matrices of eccentricity and inclination
%vectors
n = 2*pi./P; 
[A,B] = SecularInteractionsMatrix2(n,mu);

if ~isempty(t) %if at least one time stamp is required for the light curve calculation
    %calculate the secular motion of the eccentricities and inclinations   
    free_e_T = Vector1stOrderDE(A,free_e_0.',horizontal(tT)-t(1));
    free_I_T = Vector1stOrderDE(B,free_I_0.',horizontal(tT)-t(1));
end




function [AA,BB] = SecularInteractionsMatrix2(n,mu)
%calculate the interaction matrix that will describe the equation dz/dt=Az
%where z is a vector of the complex eccentricities and for the similar
%equation for I*exp(i*Omega) using the matrix BB

Npl = length(n);

AA = ones(Npl);
BB = ones(Npl);

for j1 = 1:Npl
    for j2 = j1:Npl
        
        if j1~=j2
            alph = (n(j2)/n(j1))^(2/3)*((1+mu(j1))/(1+mu(j2)))^(1/3);
            A1 = LaplaceCoeffs(alph,1/2,1);
            
            
            DA1 = LaplaceCoeffs(alph,1/2,1,1);
            D2A1 = LaplaceCoeffs(alph,1/2,1,2);
            
            f10 = 0.5*A1-0.5*alph*DA1-0.25*alph^2*D2A1; %calculate f2 and f10 for j=0
            AA(j1,j2) = n(j1)*mu(j2)/(1+mu(j1))*alph*f10;
            AA(j2,j1) = n(j2)*mu(j1)/(1+mu(j2))*f10;
            
            B1 = LaplaceCoeffs(alph,3/2,1);
            f14 = alph*B1;
            BB(j1,j2) = 0.25*n(j1)*mu(j2)/(1+mu(j1))*alph*f14;
            BB(j2,j1) = 0.25*n(j2)*mu(j1)/(1+mu(j2))*f14;
            
        else
            alphvec = zeros(1,Npl);
            alphvec(1:j1) = (n(1:j1)/n(j1)).^(-2/3).*((1+mu(1:j1))./(1+mu(j1))).^(1/3);
            alphvec(j1+1:end) = (n(j1+1:end)/n(j1)).^(2/3).*((1+mu(j1+1:end))./(1+mu(j1))).^(-1/3);
            DA0 = LaplaceCoeffs(alphvec,1/2,0,1);
            D2A0 = LaplaceCoeffs(alphvec,1/2,0,2);
            f2 = 0.25*alphvec.*DA0+1/8*alphvec.^2.*D2A0;
            InnerVec = mu(1:(j1-1))/(1+mu(j1)).*f2(1:(j1-1));
            OuterVec = alphvec((j1+1):end).*mu((j1+1):end)/(1+mu(j1)).*f2((j1+1):end);
            
            AA(j1,j2) = n(j1)*2*(sum(InnerVec)+sum(OuterVec));
            
            B1 = LaplaceCoeffs(alphvec,3/2,1);
            f3 = -0.5*alphvec.*B1;
            InnerVec = mu(1:(j1-1)).*f3(1:(j1-1));
            OuterVec = alphvec((j1+1):end).*mu((j1+1):end).*f3((j1+1):end);
            BB(j1,j2) = 0.25*n(j1)*2*(sum(InnerVec)+sum(OuterVec));
        end
    end
end

AA = 1i*AA;
BB = 1i*BB;

end

end
