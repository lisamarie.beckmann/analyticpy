function [AssociatedTmid,AssociatedInd] = AssociateTmid(t,Tmid,P)
% AssociateTmid: Associate a time-of-mid-transit to each time stamp t.

%Inputs: t - list of times
%           Tmid - list of times of mid transit
%           P - orbital period (optional input)

% Yair Judkovsky, 2.5.2020

%estimate the period if it is not given
if ~exist('P','var')
P = median(diff(Tmid));
end

%transit indices - the first transit in the data is one, and going up
try
    Tr_Ind = 1+round((Tmid-Tmid(1))/P);
catch
    AssociatedTmid = nan(size(t));
    AssociatedInd = nan(size(t));
    return
end

%associate an index of transit per time stamp. Any event before the first
%Tmid will be associated with the first Tmid. Any event after the last Tmid
%will be associated with the last Tmid.
AssociatedInd = 1+round((t-Tmid(1))/P);
AssociatedInd(AssociatedInd<1) = 1;
AssociatedInd(AssociatedInd>Tr_Ind(end)) = Tr_Ind(end);

%Associated transit index
try
Tmid_Of_Tr_Ind = inf(1,max(Tr_Ind));
Tmid_Of_Tr_Ind(Tr_Ind) = Tmid;
%next few lines for debugging purposes
catch
    save('debug_tmid');
    AssociatedTmid = nan(size(t));
    AssociatedInd = nan(size(t));
    return
end

%associate a value of Tmid for each time stamp
AssociatedTmid = Tmid_Of_Tr_Ind(AssociatedInd);